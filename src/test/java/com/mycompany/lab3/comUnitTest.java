/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author wittaya
 */
public class comUnitTest {
    
    public comUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void checkwin_vertical0_output_true() {
        char[][] table = {{'X','X','X'},{'-','-','-'},{'-','-','-'}};
        char turn = 'X';
        boolean result = Lab3.checkwin(table,turn);
        assertEquals( true,result);
    }
    @Test
    public void checkwin_vertical1_output_true() {
        char[][] table = {{'-','-','-'},{'X','X','X'},{'-','-','-'}};
        char turn = 'X';
        boolean result = Lab3.checkwin(table,turn);
        assertEquals( true,result);
    }
    @Test
    public void checkwin_vertical2_output_true() {
        char[][] table = {{'-','-','-'},{'-','-','-'},{'X','X','X'}};
        char turn = 'X';
        boolean result = Lab3.checkwin(table,turn);
        assertEquals( true,result);
    }
    @Test
    public void checkwin_Horizontal0_output_true() {
        char[][] table = {{'X','-','-'},{'X','-','-'},{'X','-','-'}};
        char turn = 'X';
        boolean result = Lab3.checkwin(table,turn);
        assertEquals( true,result);
    }
    @Test
    public void checkwin_Horizontal1_output_true() {
        char[][] table = {{'-','X','-'},{'-','X','-'},{'-','X','-'}};
        char turn = 'X';
        boolean result = Lab3.checkwin(table,turn);
        assertEquals( true,result);
    }
    @Test
    public void checkwin_Horizontal2_output_true() {
        char[][] table = {{'-','-','X'},{'-','-','X'},{'-','-','X'}};
        char turn = 'X';
        boolean result = Lab3.checkwin(table,turn);
        assertEquals( true,result);
    }
    @Test
    public void checkwin_diagonal1_output_true() {
        char[][] table = {{'-','-','X'},{'-','X','-'},{'X','-','-'}};
        char turn = 'X';
        boolean result = Lab3.checkwin(table,turn);
        assertEquals( true,result);
    }
    @Test
    public void checkwin_diagonal2_output_true() {
        char[][] table = {{'X','-','-'},{'-','X','-'},{'-','-','X'}};
        char turn = 'X';
        boolean result = Lab3.checkwin(table,turn);
        assertEquals( true,result);
    }
    @Test
    public void checkdraw() {
        char[][] table = {{'X','O','X'},
                          {'O','O','X'},
                          {'X','X','O'}};
        boolean result = Lab3.checkdraw(table);
        assertEquals( true,result);
    }
}
