/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3;

/**
 *
 * @author wittaya
 */
public class Lab3 {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
    
    static int add(int i, int j) {
        int result = i+j;
        return result;
    }

    static boolean checkwin(char[][] table, char turn) {
        for(int i=0;i<3;i++){
            if(checkrow(i,table,turn)){
                return true;
            }
        }
        for(int j=0;j<3;j++){
            if(checkcol(j,table,turn)){
                return true;
            }
        }
        if(checkdiagonal1(table,turn)){
                return true;
            }
        if(checkdiagonal2(table,turn)){
                return true;
            }
        return false;
    }

    private static boolean checkrow(int i,char[][] table, char turn) {
        return table[i][0] == turn && table[i][1] == turn && table[i][2] == turn;
    }
    private static boolean checkcol(int j,char[][] table, char turn) {
        return table[0][j] == turn && table[0][j] == turn && table[0][j] == turn;
    }
    private static boolean checkdiagonal1(char[][] table, char turn) {
        return table[0][0] == turn && table[1][1] == turn && table[2][2] == turn;
    }
    private static boolean checkdiagonal2(char[][] table, char turn) {
        return table[0][2] == turn && table[1][1] == turn && table[2][0] == turn;
    }
    static boolean checkdraw(char[][] table){
        if(tcheckdraw(table)){
                return true;
            }
        
        return false;
    }
    
    private static boolean tcheckdraw(char[][] table) {
        for(int i =0;i<3;i++){
            for(int j =0;j<3;j++){
                if(table[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
    }
}
